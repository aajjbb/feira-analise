callAjax = function(curso, data, action) {
    $.ajax({
        url: 'api.php',
        type: 'get',
        data: {
            'action': action,
            'curso': curso,
            'data': data
        },

        success: function(data, status) {
            var obj = JSON.parse(data);

            $('#inscritosCell').html(obj.inscritos);
            $('#presentesCell').html(obj.presentes);
        },
        error: function(xhr, desc, err) {
            $('#inscritosCell').html(0);
            $('#presentesCell').html(0);

            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });
};

$(document).ready(function() {
    callAjax($('#selectCurso').find(":selected").text(),
             $('#selectData').find(":selected").text(),
             'countAll');

    $('.dropdown').dropdown({
        onChange: function(value, text, $selectedItem) {
            var curso = $('#selectCurso').find(":selected").text();
            var data = $('#selectData').find(":selected").text();

            callAjax(curso, data, 'countAll');
        }
    });
    $('table').tablesort();
    $('.menu .item').tab();
});
