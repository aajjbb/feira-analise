<html>
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
    <script
        src="js/jquery-3.2.1.min.js"></script>
    <script src="semantic/semantic.min.js"></script>
    <script src="js/index.js"></script>

    <link rel="stylesheet" type="text/css" href="css/index.css">
  </head>
  <body>
    <div class="ui four column centered grid centered main container">
    <div class="ui inverted segment">
      <div class="ui inverted secondary  menu">
        <a class="item" href="index.php">
          Contagem Geral
        </a>
        <a class="item" href="index-porCurso.php">
          Por Curso
        </a>
        <a class="item" href="index-porEscola.php">
          Por Escola
        </a>
        <a class="item" href="index-porMunicipio.php">
          Por Municipio
        </a>
      </div>
    </div>
      <div class="two column row">
        <div class="column">
          <h3 class="ui block center aligned header">
            Contagem de Inscritos/Presentes
          </h3>
        </div>
      </div>
      <div class="four column row">
        <div class="column">
          <h3 class="ui block center aligned header">
            Curso
          </h3>
        </div>
        <div class="column">
          <h3 class="ui block center aligned header">
            Data
          </h3>
        </div>
      </div>
      <div class="four column row">
        <div id="selectCurso" class="column">
          <select class="ui fluid search selection dropdown">
            <option value="">Todos</option>
            <option value="Administração">Administração</option>
            <option value="Agronomia">Agronomia</option>
            <option value="Análise e Desenvolvimento de Sistemas">Análise e Desenvolvimento de Sistemas</option>
            <option value="Arquitetura e Urbanismo">Arquitetura e Urbanismo</option>
            <option value="Ciências Biológicas">Ciências Biológicas</option>
            <option value="Ciências Contábeis">Ciências Contábeis</option>
            <option value="Ciências Econômicas">Ciências Econômicas</option>
            <option value="Comércio Exterior">Comércio Exterior</option>
            <option value="Direito">Direito</option>
            <option value="Educação Física">Educação Física</option>
            <option value="Enfermagem">Enfermagem</option>
            <option value="Engenharia Aeronáutica">Engenharia Aeronáutica</option>
            <option value="Engenharia Ambiental e Sanitária">Engenharia Ambiental e Sanitária</option>
            <option value="Engenharia Civil">Engenharia Civil</option>
            <option value="Engenharia de Alimentos">Engenharia de Alimentos</option>
            <option value="Engenharia de Computação">Engenharia de Computação</option>
            <option value="Engenharia de Controle e Automação">Engenharia de Controle e Automação</option>
            <option value="Engenharia de Produção Mecânica">Engenharia de Produção Mecânica</option>
            <option value="Engenharia Elétrica e Eletrônica">Engenharia Elétrica e Eletrônica</option>
            <option value="Engenharia Mecânica">Engenharia Mecânica</option>
            <option value="Estética e Cosmética">Estética e Cosmética</option>
            <option value="Física">Física</option>
            <option value="Fisioterapia">Fisioterapia</option>
            <option value="Geografia">Geografia</option>
            <option value="Gestão de Recursos Humanos">Gestão de Recursos Humanos</option>
            <option value="História">História</option>
            <option value="Jornalismo">Jornalismo</option>
            <option value="Letras">Letras</option>
            <option value="Matemática">Matemática</option>
            <option value="Medicina">Medicina</option>
            <option value="Nutrição">Nutrição</option>
            <option value="Odontologia">Odontologia</option>
            <option value="Pedagogia">Pedagogia</option>
            <option value="Processos Gerenciais">Processos Gerenciais</option>
            <option value="Psicologia">Psicologia</option>
            <option value="Publicidade e Propaganda">Publicidade e Propaganda</option>
            <option value="Química">Química</option>
            <option value="Relações Públicas">Relações Públicas</option>
            <option value="Serviço Social">Serviço Social</option>
            <option value="Sistemas de Informação">Sistemas de Informação</option>
          </select>
        </div>
        <div class="column">
          <select id="selectData" class="ui fluid search selection dropdown">
            <option value="">Todos</option>
            <option value="1">2017-09-18</option>
            <option value="0">2017-09-19</option>
          </select>
        </div>
      </div>
      <div class="two column row">
        <div class="column">
          <table class="ui celled sortable table">
            <thead>
              <th>
                Inscritos
              </th>
              <th>
                Presentes
              </th>
            </thead>
            <tbody>
              <tr>
                <td id="inscritosCell">
                </td>
                <td id="presentesCell">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
