<html>
  <head>
    <meta charset="utf-8" /> 
    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
    <script
        src="js/jquery-3.2.1.min.js"></script>
    <script src="semantic/semantic.min.js"></script>
    <script src="js/index-porCurso.js"></script>

    <link rel="stylesheet" type="text/css" href="css/index.css">
  </head>
  <body>
    <div class="ui four column centered grid centered main container">
    <div class="ui inverted segment">
      <div class="ui inverted secondary  menu">
        <a class="item" href="index.php">
          Contagem Geral
        </a>
        <a class="item" href="index-porCurso.php">
          Por Curso
        </a>
        <a class="item" href="index-porEscola.php">
          Por Escola
        </a>
        <a class="item" href="index-porMunicipio.php">
          Por Municipio
        </a>
      </div>
    </div>
      <div class="two column row">
        <div class="column">
          <h3 class="ui block center aligned header">
            Contagem de Inscritos/Presentes - Por Curso
          </h3>
        </div>
      </div>
      <div class="four column row">
        <div class="column">
          <h3 class="ui block center aligned header">
            Data
          </h3>
        </div>
        <div class="column">
          <h3 class="ui block center aligned header">
            Periodo
          </h3>
        </div>
        <div class="column">
          <h3 class="ui block center aligned header">
            Cidade
          </h3>
        </div>
        <div class="column">
          <h3 class="ui block center aligned header">
            Escola
          </h3>
        </div>
      </div>
      <div class="four column row">
      <div class="column">
        <select id="selectData" class="ui fluid search selection dropdown">
            <option value="Todos">Todos</option>
            <option value="1">2017-09-18</option>
            <option value="0">2017-09-19</option>
          </select>
        </div>
        <div class="column">
          <select id="selectPeriodo" class="ui fluid search selection dropdown">
            <option value="Todos">Todos</option>
            <option value="1">Manhã</option>
            <option value="0">Noite</option>
          </select>
        </div>
        <div id="selectCidade" class="column">
          <select class="ui fluid search selection dropdown">
            <option value="Todos">Todos</option>
            <?php
              require_once("connection.php");
              $connection = connectMysql();

              $query = "SELECT cidade FROM escola GROUP BY cidade ORDER BY cidade";

              $result = $connection->query($query);

              while ($values = $result->fetch_assoc()) {
                echo "<option value=\"" . $values["cidade"] .  "\">" . $values["cidade"] . "</option>\n";
              }
              $result->free();
              $connection->close();
            ?>
          </select>
        </div>
        <div id="selectEscola" class="column">
          <select class="ui fluid search selection dropdown">
            <option value="Todos">Todos</option>
            <?php
              require_once("connection.php");
              $connection = connectMysql();

              $query = "SELECT escola FROM escola ORDER BY escola";

              $result = $connection->query($query);

              while ($values = $result->fetch_assoc()) {
                echo "<option value=\"" . $values["escola"] .  "\">" . $values["escola"] . "</option>\n";
              }
              $result->free();
              $connection->close();
            ?>
          </select>
        </div>
      </div>
      <div class="two column row">
        <div class="column">
          <table id="table" class="ui celled sortable table">
            <thead>
              <th>
                Índice
              </th>
              <th>
                Curso
              </th>
              <th>
                Inscritos
              </th>
              <th>
                Presentes
              </th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
