<?php require_once("connection.php"); ?>
<?php

$action = $_GET["action"];

switch ($action) {
    case 'countAll':
        countAll();
        break;
    case 'porCurso':
        porCurso();
        break;
    case 'porEscola':
        porEscola();
        break;
    case 'porMunicipio':
        porMunicipio();
        break;
    default:
}

// Retorna '%' caso o Campo esteja vazio, sendo que '%' em SQL é um matcher universal.
function cleanField($field) {
    if ($field == "" or $field == "Todos") {
        return "%";
    }
    return $field;
}

function countAll() {
    $curso = cleanField($_GET['curso']);
    $data  = cleanField($_GET['data']);

    $connection = connectMysql();
    $queryInscritos = "SELECT count(*) as inscritos FROM aluno WHERE curso_interesse LIKE '$curso' AND data LIKE '$data'";
    $queryPresentes = "SELECT count(*) as presentes FROM presenca JOIN aluno ON presenca.id_aluno = aluno.id AND aluno.data = $data AND aluno.curso_interesse LIKE '$curso'";

    $inscritos = 0;
    $presentes = 0;

    if ($result = $connection->query($queryInscritos)) {
        $values = $result->fetch_assoc();

        $inscritos = $values['inscritos'];
    }
    if ($result = $connection->query($queryPresentes)) {
        $values = $result->fetch_assoc();

        $presentes = $values['presentes'];
    }

    $response = array();

    $response['inscritos'] = $inscritos;
    $response['presentes'] = $presentes;

    echo json_encode($response);

    $connection->close();
}

function porCurso() {
    $data    = cleanField($_GET['data']);
    $periodo = cleanField($_GET['periodo']);
    $cidade  = cleanField($_GET['cidade']);
    $escola  = cleanField($_GET['escola']);

    $connection = connectMysql();

    $query = "SELECT * FROM
                (SELECT curso_interesse, COUNT(CASE WHEN data LIKE '$data' AND
                periodo LIKE '$periodo' THEN 1 ELSE NULL END) AS inscritos,
                COUNT( CASE WHEN EXISTS( SELECT * FROM presenca WHERE id_aluno=aluno.id AND
                data LIKE '$data' AND periodo LIKE '$periodo' ) THEN 1 ELSE NULL END) AS presentes
                FROM aluno WHERE cidade LIKE '$cidade' AND escola LIKE '$escola' GROUP BY curso_interesse) AS foo
                WHERE inscritos>0 OR presentes>0";

    $response = array();

    if ($result = $connection->query($query)) {
        while ($values = $result->fetch_assoc()) {
            $row = array();

            $row["curso"]     = $values["curso_interesse"];
            $row["inscritos"] = $values["inscritos"];
            $row["presentes"] = $values["presentes"];

            array_push($response, $row);
        }
    }

    echo json_encode($response);

    $connection->close();
}

function porEscola() {
    $data    = cleanField($_GET['data']);
    $periodo = cleanField($_GET['periodo']);
    $cidade  = cleanField($_GET['cidade']);
    $curso   = cleanField($_GET['curso']);

    $connection = connectMysql();

    $query = "SELECT * FROM
                (SELECT escola, COUNT(CASE WHEN data LIKE '$data' AND
                periodo LIKE '$periodo' THEN 1 ELSE NULL END) AS inscritos,
                COUNT( CASE WHEN EXISTS( SELECT * FROM presenca WHERE id_aluno=aluno.id AND
                data LIKE '$data' AND periodo LIKE '$periodo' ) THEN 1 ELSE NULL END) AS presentes
                FROM aluno WHERE cidade LIKE '$cidade' AND curso_interesse LIKE '$curso' GROUP BY escola) AS foo
                WHERE inscritos>0 OR presentes>0";

    $response = array();

    if ($result = $connection->query($query)) {
        while ($values = $result->fetch_assoc()) {
            $row = array();

            $row["escola"]    = $values["escola"];
            $row["inscritos"] = $values["inscritos"];
            $row["presentes"] = $values["presentes"];

            array_push($response, $row);
        }
    }

    echo json_encode($response);

    $connection->close();
}

function porMunicipio() {
    $data    = cleanField($_GET['data']);
    $periodo = cleanField($_GET['periodo']);
    $curso   = cleanField($_GET['curso']);

    $connection = connectMysql();

    $query = "SELECT * FROM
                (SELECT cidade, COUNT(CASE WHEN data LIKE '$data' AND
                periodo LIKE '$periodo' THEN 1 ELSE NULL END) AS inscritos,
                COUNT( CASE WHEN EXISTS( SELECT * FROM presenca WHERE id_aluno=aluno.id AND
                data LIKE '$data' AND periodo LIKE '$periodo' ) THEN 1 ELSE NULL END) AS presentes
                FROM aluno WHERE curso_interesse LIKE '$curso' GROUP BY cidade) AS foo
                WHERE inscritos>0 OR presentes>0";

    $response = array();

    if ($result = $connection->query($query)) {
        while ($values = $result->fetch_assoc()) {
            $row = array();

            $row["cidade"] = $values["cidade"];
            $row["inscritos"] = $values["inscritos"];
            $row["presentes"] = $values["presentes"];

            array_push($response, $row);
        }
    }

    echo json_encode($response);

    $connection->close();
}
?>